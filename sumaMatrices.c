#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"

#define filas 4
#define columnas 3
#define tamano (filas*columnas)


void mostrar(int *a, int *b, int *c){
	printf("\n");
	int i=0;
	while( i<10){
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
		i++;
	}
}

void mostrarMat(int *a){
	int i=0,j=0;
	
	for(i=0;i<filas;i++){
		for(j=0;j<columnas;j++){
			printf("%d ", a[i*columnas+j]);
		}
		printf("\n");
	}
}


int main(){
	clock_t begin, end;
	double time_spent;
	
	
	int *matrizA=NULL;
	matrizA=(int *) malloc(sizeof(int)*tamano);
	int *matrizB=NULL;
	matrizB=(int *) malloc(sizeof(int)*tamano);
	int *matrizC=NULL;
	matrizC=(int *) malloc(sizeof(int)*tamano);
	
	
	int i=0, j=0;
	
	
	while(i<tamano){
		matrizA[i]=rand()%10;
		matrizB[i]=rand()%10;
		i++;
	}
	
	//begin = clock();
	
	for(i=0;i<filas*columnas;i++){
		for(j=0;j<columnas;j++){
			matrizC[i*columnas+j]=matrizA[i*columnas+j]+matrizB[i*columnas+j];
		}
	}
	mostrarMat(matrizA);
	printf("\n");
	mostrarMat(matrizB);
	printf("\n");
	mostrarMat(matrizC);
	printf("\n");
	//end = clock();
	
	

	//mostrar(matrizA, matriZB, matrizC);
	
  //time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  //printf("Se ha demorado %f segundos.\n",time_spent);
	
	return 0;
}
