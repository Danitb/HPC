#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cstdlib>
#include <time.h>

#define tamano 100
#define TILE_SIZE 4

#define MAX_MASK_WIDTH 5
__constant__ int M[MAX_MASK_WIDTH];


__global__ void conHalo(int *N, int *P, int Mask_Width, int Width){
  
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  __shared__ float N_ds[TILE_SIZE + MAX_MASK_WIDTH -1];

  int n = Mask_Width/2;

  int halo_index_left = (blockIdx.x - 1)*blockDim.x + threadIdx.x;
  if (threadIdx.x >= blockDim.x - n) {
    N_ds[threadIdx.x - (blockDim.x -n)] = (halo_index_left < 0) ? 0 : N[halo_index_left];
  }

  N_ds[n + threadIdx.x] = N[blockIdx.x*blockDim.x + threadIdx.x];

  int halo_index_right = (blockIdx.x + 1)*blockDim.x + threadIdx.x;
  if (threadIdx.x < n) {
    N_ds[n + blockDim.x + threadIdx.x] = (halo_index_right >= Width) ? 0 : N[halo_index_right];
  }

  __syncthreads();

  float Pvalue = 0;
  for (int j=0; j < Mask_Width;j++){
    Pvalue += N_ds[threadIdx.x +j]*M[j];
  }
  P[i] = Pvalue;

}

__global__ void conCaching(int *N, int *P, int Mask_Width, int Width){
  int i = blockIdx.x*blockDim.x + threadIdx.x;

  float Pvalue = 0;
  int N_start_point = i- (Mask_Width/2);
  for (int j = 0;j < Mask_Width;j++){
    if(N_start_point + j >= 0 && N_start_point + j < Width){
      Pvalue += N[N_start_point +j]*M[j];
    }
  }
  P[i] = Pvalue;
}


__global__ void conBasica(int *N, int *M, int *P, int Mask_Width, int Width){
  int i = blockIdx.x*blockDim.x + threadIdx.x;

  float Pvalue = 0;
  int N_start_point = i- (Mask_Width/2);
  for (int j = 0;j < Mask_Width;j++){
    if(N_start_point + j >= 0 && N_start_point + j < Width){
      Pvalue += N[N_start_point +j]*M[j];
    }
  }
  P[i] = Pvalue;
}


void callCuda(int *W, int *B, int *Rp, int *Rpc, int *Rpct, int backTam, int winTam){
    int sizeW= winTam*sizeof(int);
    int sizeB= backTam*sizeof(int);
  int sizeR= (winTam+backTam)*sizeof(int);
  
  int *d_W, *d_WC, *d_B, *d_R, *d_RC, *d_RCT;
  cudaMalloc((void **)&d_W,sizeW);  //reserva memoria en el device
  cudaMalloc((void **)&d_WC,sizeW); //reserva memoria en el device

  cudaMalloc((void **)&d_B,sizeB);

  cudaMalloc((void **)&d_R,sizeR);
  cudaMalloc((void **)&d_RC,sizeR);
  cudaMalloc((void **)&d_RCT,sizeR);

  int numBlockX=32;
  while(backTam%numBlockX!=0){
    numBlockX=numBlockX-1;
  }
  
    clock_t t2;
    t2 = clock(); //tiempo de asignacion de memoria
  cudaMemcpy( d_W, W, sizeW, cudaMemcpyHostToDevice); //se copian al device

  cudaMemcpy( d_B, B, sizeB, cudaMemcpyHostToDevice);

  dim3 dimBlock(numBlockX,1,1);     //->3es (bloques en X y Y?)
    dim3 dimGrid(ceil(backTam/dimBlock.x),1,1); //100 bloques en X y 100 bloques en Y
    t2 = clock() - t2;

    clock_t t3;
    t3 = clock();
  conBasica<<< dimGrid, dimBlock >>>(d_B, d_W, d_R, winTam, backTam);
  cudaMemcpy( Rp,d_R, sizeR, cudaMemcpyDeviceToHost);
  t3 = clock() - t3;
  
  printf ("\nTiempo con la convolucion basica: %f segundos\n",((float)(t2+t3))/CLOCKS_PER_SEC);

  clock_t t4;
    t4 = clock();
  cudaMemcpy( d_WC, W, sizeW, cudaMemcpyHostToDevice);  //se copian al device
    cudaMemcpyToSymbol(M,d_WC,winTam*sizeof(int));
  conCaching<<< dimGrid, dimBlock >>>(d_B, d_RC, winTam, backTam);
  cudaMemcpy( Rpc,d_RC, sizeR, cudaMemcpyDeviceToHost);
  t4 = clock() - t4;

    printf ("\nTiempo con la convolucion con caching: %f segundos\n",((float)(t2+t4))/CLOCKS_PER_SEC);

    clock_t t5;
    t5 = clock();
  cudaMemcpy( d_WC, W, sizeW, cudaMemcpyHostToDevice);  //se copian al device
    cudaMemcpyToSymbol(M,d_WC,winTam*sizeof(int));
  conHalo<<< dimGrid, dimBlock >>>(d_B, d_RCT, winTam, backTam);
  cudaMemcpy( Rpct,d_RCT, sizeR, cudaMemcpyDeviceToHost);
  t5 = clock() - t5;

    printf ("\nTiempo con halo elements: %f segundos\n",((float)(t2+t5))/CLOCKS_PER_SEC);

  cudaFree(d_W);      //libera memoria del dispositivo
  cudaFree(d_B);
  cudaFree(d_R);
  cudaFree(d_RC);

}

 
int main(){
  int * Ventana;  
  int * vector;

  int * Rp;
  int * Rpc;
  int * Rpct;

    int winTam=MAX_MASK_WIDTH;    

  Ventana = (int*)malloc( winTam*sizeof(int) );
  vector = (int*)malloc( tamano*sizeof(int) );

    Rp = (int*)malloc( (tamano+winTam)*sizeof(int) );
    Rpc = (int*)malloc( (tamano+winTam)*sizeof(int) );
    Rpct = (int*)malloc( (tamano+winTam)*sizeof(int) );

  for(int i=0;i<winTam;i++){
    Ventana[i]=rand()%10;
  }


  for(int i=0;i<tamano;i++){
    vector[i]=rand()%10;
  }
      
    callCuda(Ventana, vector, Rp, Rpc, Rpct, tamano, winTam);
    free(Ventana);
    free(vector);
    free(Rp);
    free(Rpc);
    free(Rpct);

  return 0;
}