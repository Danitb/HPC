
#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "cuda.h"

#define filasA 512
#define columA 512
#define filasB 512
#define columB 512


void llenarMat(int *a, int numero){
	int i=0;
	
	while(i<numero){
		a[i]=1;//rand()%10;
		i++;
	}
}


void mostrarMat(int *a, int f, int c){
	int i=0,j=0;
	
	for(i=0;i<f;i++){
		for(j=0;j<c;j++){
			printf("%d ", a[i*c+j]);
		}
		printf("\n");
	}
}


void multisecuencial(int *matA, int *matB, int *matC){
	int i, j, k, cont;
	clock_t begin, end;
	double time_spent;
	begin = clock();
	
	for(i=0;i<filasA;i++){
		for(j=0;j<columB;j++){
			cont=0;
			for(k=0;k<columA;k++){
				cont+= matA[i*columA+k]*matB[k*columB+j];
			}
			matC[i*columB+j]=cont;
		}
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado la multiplicacion secuencial %f segundos.\n",time_spent);
}


__global__ void matMulti(int *matA, int *matB, int *matC){
  
	int i=blockIdx.y*blockDim.y+threadIdx.y;   //me representa el numero de la fila 
	int j=blockIdx.x*blockDim.x+threadIdx.x;   //me representa el numero de la columna
	int cont,k;
  
  
	if((i<filasA) && (j<columB)){
		//matrizC[f*columnas+c] = matrizA[f*columnas+c] + matrizB[f*columnas+c];
		cont=0;
			for(k=0;k<columA;k++){
				cont+= matA[i*columA+k]*matB[k*columB+j];
			}
			matC[i*columB+j]=cont;
	}
}

void matM(int *A, int *B, int *C){
	int sizeA=(filasA*columA)*sizeof(int);
	int sizeB=(filasB*columB)*sizeof(int);
	int sizeC=(filasA*columB)*sizeof(int);
	int *d_A, *d_B, *d_C;
	
  clock_t begin, end;
	double time_spent;
  
  multisecuencial(A,B,C);
  
  
  dim3 blockSize(32,32);
  //dim3 blockSize(1000,1000);
  dim3 dimGrid((int)ceil((float)(filasA)/(float)blockSize.x),(int)ceil((float)(columB)/(float)blockSize.y));
  //dim3 dimGrid(1000,1000);
 
 
	
	
	//Ejecucion en GPU
	begin = clock();
  
	cudaMalloc((void**)&d_A, sizeA);
	cudaMalloc((void**)&d_B, sizeB);
	cudaMalloc((void**)&d_C, sizeC);
	

	
	cudaMemcpy(d_A, A, sizeA, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, sizeB, cudaMemcpyHostToDevice);
	
	
	matMulti<<<dimGrid,blockSize>>>(d_A, d_B, d_C);
	
	
	cudaMemcpy(C, d_C, sizeC, cudaMemcpyDeviceToHost);
  end = clock();
  
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado la multipliacion en GPU %f segundos.\n",time_spent);
  mostrarMat(C, 20, 20);
	printf("\n");
  
	//mostrar(A,B,C);
	//me_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	//intf("Se ha demorado la suma en GPU %f segundos.\n",time_spent);
	
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	//return 0;
}

int main(){
	
	int *matrizA=NULL;
	matrizA=(int *) malloc(sizeof(int)*(filasA*columA));
	int *matrizB=NULL;
	matrizB=(int *) malloc(sizeof(int)*(filasB*columB));
	int *matrizC=NULL;
	matrizC=(int *) malloc(sizeof(int)*(filasA*columB));
	
	llenarMat(matrizA, filasA*columA);
	llenarMat(matrizB, filasB*columB);
	
	matM(matrizA, matrizB, matrizC);
	return 0;
}		
