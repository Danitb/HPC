#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "cuda.h"
#define tamano 500000


void mostrar(float *a, float *b, float *c){
	printf("\n");
	int i=0;
	while( i<10){
		printf("%f + %f = %f\n", a[i], b[i], c[i]);
		i++;
	}
}

void sumaSecuencial(float *vectorA, float *vectorB, float *vectorC){
	
	clock_t begin, end;
	double time_spent;
	
	int i=0;
	begin = clock();
	for(i=0;i<tamano;i++){
		vectorC[i]=vectorA[i]+vectorB[i];
	}
	end = clock();

	//mostrar(vectorA, vectorB, vectorC);
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado la suma secuencial %f segundos.\n",time_spent);
}


__global__ void vecAdd(float *A, float *B, float *C, int n){
	//int i=threadIdx.x;
	int i=blockIdx.x*blockDim.x+threadIdx.x;
	if(i<n)
		C[i]=A[i]+B[i];
}

void vectorAdd(float *A, float *B, float *C, int n){
	int size=n*sizeof(int);
	float *d_A, *d_B, *d_C;
	clock_t begin, end;
	double time_spent;
	int blockSize=1024;
	int dimGrid=(int)ceil((float)tamano/blockSize);
	
	
	//Ejecucion en CPU
	sumaSecuencial(A, B, C);
	
	//Ejecucion en GPU
	
  
	cudaMalloc((void**)&d_A, size);
	cudaMalloc((void**)&d_B, size);
	cudaMalloc((void**)&d_C, size);
	

	
	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);
	
	begin = clock();
	vecAdd<<<dimGrid,blockSize>>>(d_A, d_B, d_C, n);
	end = clock();
	
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);
	//mostrar(A,B,C);
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado la suma en GPU %f segundos.\n",time_spent);
	
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	//return 0;
}

int main(){
	int i=0;
	
	float *vectorA=NULL;
	vectorA=(float *) malloc(sizeof(float)*tamano);
	float *vectorB=NULL;
	vectorB=(float *) malloc(sizeof(float)*tamano);
	float *vectorC=NULL;
	vectorC=(float *) malloc(sizeof(float)*tamano);
	
	while(i<tamano){
		vectorA[i]=rand()%10;
		vectorB	[i]=rand()%10;
		i++;
	}
	vectorAdd(vectorA, vectorB, vectorC, tamano);
	return 0;
}
