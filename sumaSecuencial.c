#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"

#define tamano 10

void mostrar(int *a, int *b, int *c){
	printf("\n");
	int i=0;
	while( i<10){
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
		i++;
	}
}


int main(){
	clock_t begin, end;
	double time_spent;
	
	
	int *vectorA=NULL;
	vectorA=(int *) malloc(sizeof(int)*tamano);
	int *vectorB=NULL;
	vectorB=(int *) malloc(sizeof(int)*tamano);
	int *vectorC=NULL;
	vectorC=(int *) malloc(sizeof(int)*tamano);
	
	
	int i=0;
	
	
	while(i<tamano){
		vectorA[i]=rand()%10;
		vectorB[i]=rand()%10;
		i++;
	}
	begin = clock();
	for(i=0;i<tamano;i++){
		vectorC[i]=vectorA[i]+vectorB[i];
	}
	end = clock();

	mostrar(vectorA, vectorB, vectorC);
	
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("Se ha demorado %f segundos.\n",time_spent);
	
	return 0;
}
