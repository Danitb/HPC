#include <cv.h>
#include <highgui.h>
#include <time.h>
#include <cuda.h>

#define RED 2
#define GREEN 1
#define BLUE 0

using namespace cv;


__device__ unsigned char clamp(int value){
    if(value < 0)
        value = 0;
    else
        if(value > 255)
            value = 255;
    return (unsigned char)value;
}

__global__ void Union(unsigned char *Sobel_X, unsigned char *Sobel_Y, int width, int height, unsigned char *imageOutput){
    int row = blockIdx.y*blockDim.y+threadIdx.y;
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    if((row < height) && (col < width)){
      imageOutput[row*width+col]= clamp(sqrtf( Sobel_X[row*width+col]*Sobel_X[row*width+col]+Sobel_Y[row*width+col]*Sobel_Y[row*width+col]) );
    }
}

__global__ void sobelFilter(unsigned char *imageInput, int width, int height, unsigned int maskWidth, char *M,unsigned char *imageOutput){
    unsigned int row = blockIdx.y*blockDim.y+threadIdx.y;
    unsigned int col = blockIdx.x*blockDim.x+threadIdx.x;

    int Pvalue = 0;

    int N_start_point_row = row - (maskWidth/2);
    int N_start_point_col = col - (maskWidth/2);

    for(int i = 0; i < maskWidth; i++){
        for(int j = 0; j < maskWidth; j++ ){
            if((N_start_point_col + j >=0 && N_start_point_col + j < width) \
                    &&(N_start_point_row + i >=0 && N_start_point_row + i < height)){
                Pvalue += imageInput[(N_start_point_row + i)*width+(N_start_point_col + j)] * M[i*maskWidth+j];
            }
        }
    }
    imageOutput[row*width+col] = clamp(Pvalue);
}

__global__ void img2gray(unsigned char *imageInput, int width, int height, unsigned char *imageOutput){
    int row = blockIdx.y*blockDim.y+threadIdx.y;
    int col = blockIdx.x*blockDim.x+threadIdx.x;

    if((row < height) && (col < width)){
        imageOutput[row*width+col] = imageInput[(row*width+col)*3+RED]*0.299 + imageInput[(row*width+col)*3+GREEN]*0.587 \
                                     + imageInput[(row*width+col)*3+BLUE]*0.114;
    }
}


int filtroGlobal(){
    
    char h_M[] = {-1,0,1,-2,0,2,-1,0,1}, *d_M, h_MY[] = {-1,-2,-1,0,0,0,1,2,1}, *d_MY;
    unsigned char *dataRawImage, *d_dataRawImage, *d_imageOutput, *h_imageOutput, *d_sobelOutput, *d_sobelOutput2, *h_imageOutput2;
    Mat image;
    image = imread( "./inputs/img1.jpg" );
    if(!image.data){
        printf("No image Data \n");
        return -1;
    }

    Size s = image.size();

    int width = s.width;
    int height = s.height;
    int size = sizeof(unsigned char)*width*height*image.channels();
    int sizeGray = sizeof(unsigned char)*width*height;


    dataRawImage = (unsigned char*)malloc(size);
    cudaMalloc((void**)&d_dataRawImage,size);
    

    h_imageOutput = (unsigned char *)malloc(sizeGray);
    h_imageOutput2 = (unsigned char *)malloc(sizeGray);
    cudaMalloc((void**)&d_imageOutput,sizeGray);
    

    cudaMalloc((void**)&d_M,sizeof(char)*9);
  cudaMalloc((void**)&d_MY,sizeof(char)*9);
    

    cudaMalloc((void**)&d_sobelOutput,sizeGray);
  cudaMalloc((void**)&d_sobelOutput2,sizeGray);
    
    dataRawImage = image.data;

    
    cudaMemcpy(d_dataRawImage,dataRawImage,size, cudaMemcpyHostToDevice);
    
    cudaMemcpy(d_M,h_M,sizeof(char)*9, cudaMemcpyHostToDevice);
    cudaMemcpy(d_MY,h_MY,sizeof(char)*9, cudaMemcpyHostToDevice);
    
    int blockSize = 32;
    dim3 dimBlock(blockSize,blockSize,1);
    dim3 dimGrid(ceil(width/float(blockSize)),ceil(height/float(blockSize)),1);
    img2gray<<<dimGrid,dimBlock>>>(d_dataRawImage,width,height,d_imageOutput);
    cudaDeviceSynchronize();
    sobelFilter<<<dimGrid,dimBlock>>>(d_imageOutput,width,height,3,d_M,d_sobelOutput);
    cudaDeviceSynchronize();
    //cudaMemcpy(h_imageOutput,d_sobelOutput,sizeGray,cudaMemcpyDeviceToHost);
    sobelFilter<<<dimGrid,dimBlock>>>(d_imageOutput,width,height,3,d_MY,d_sobelOutput2);
  
    cudaDeviceSynchronize();
  
    Union<<<dimGrid,dimBlock>>>(d_sobelOutput,d_sobelOutput2,width,height,d_sobelOutput);
    
  cudaMemcpy(h_imageOutput,d_sobelOutput,sizeGray,cudaMemcpyDeviceToHost);
    
    

    Mat gray_image;
    gray_image.create(height,width,CV_8UC1);
    gray_image.data = h_imageOutput;

    

    imwrite("./outputs/1054996224.png",gray_image);

    cudaFree(d_dataRawImage);
    cudaFree(d_imageOutput);
    cudaFree(d_M);
    cudaFree(d_sobelOutput);
    return 0;
}

int main(){
  int i=0;
  
  clock_t begin, end;
  double time_spent;
  
  
  for(i=0;i<20;i++){
    begin = clock();
    filtroGlobal();
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%f\n",time_spent);
  }
  return 0;
}