//Daniel Torres Bedoya HPC

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cstdlib>
#include <time.h>
#define tamano 8

//Funcion que me permite llenar el vector con numero aleatorios
void llenar(int *vec){
  int i=0;
  while(i<tamano){
    vec[i]= 1;//rand()%10;
    i++;
  }
}

//Funcion que me permite sumar de manera secuencial
void sumar(int *vector){
  clock_t begin, end;
  double time_spent;
  begin = clock();
  
  int cont=0, i=0;
  while(i<tamano){
    cont=cont+vector[i];
    i++;
  }
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("El resultado de la suma del vector de forma secuencial es: %d\n", cont);
  printf("  con un tiempo de %f segundos.\n",time_spent);

}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Funciones para realizar algoritmo de reduccion: Interleaved Addressing 

__global__ void reducir1(int *d_M, int *d_P, int Width){

  __shared__ float partialSum[tamano];

  unsigned int t = threadIdx.x;
  unsigned int i = blockIdx.x*blockDim.x+ threadIdx.x;
  partialSum[t] = d_M[i];
  __syncthreads();
  
  for (unsigned int stride = 1; stride < blockDim.x; stride *= 2)
  { 
      if(t%(2*stride)==0){
        partialSum[t] += partialSum[t+stride];
      }
      __syncthreads();
    
  }
  if(t==0){
     d_P[blockIdx.x]=partialSum[0];
  }

}


void reducirp(int *A, int *C){
  int size= tamano*sizeof(int);
  int *d_A;
  int *d_C;
  
  cudaMalloc((void **)&d_A,size);
  cudaMalloc((void **)&d_C,size); 

  cudaMemcpy( d_A, A, size, cudaMemcpyHostToDevice);
  int blocksize =1024;

  int dimGrid= (int)ceil((float)tamano/(float)blocksize);
  int n = tamano;
  
  reducir1<<<  dimGrid, tamano >>>(d_A, d_C, n);
  cudaMemcpy( C,d_C, size, cudaMemcpyDeviceToHost);  
  
  cudaFree(d_A);
  cudaFree(d_C);
  
}
////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////
//Funciones para realizar algoritmo de reduccion: Sequential Addressing 

__global__ void reducir2(int *d_M, int *d_P, int Width){

  __shared__ float partialSum[tamano];

  unsigned int t = threadIdx.x;
  unsigned int i = blockIdx.x*blockDim.x+ threadIdx.x;
  partialSum[t] = d_M[i];
  __syncthreads();
  
  for (unsigned int stride = blockDim.x/2; stride > 0; stride >>=1)
  { 
    if(t<stride){
      partialSum[t] += partialSum[t+stride];
    }
    __syncthreads();
    
  }
  if(t==0){
     d_P[blockIdx.x]=partialSum[0];
  }

}


void reducirs(int *A, int *C){
  int size= tamano*sizeof(int);
  int *d_A;
  int *d_C;
  
  cudaMalloc((void **)&d_A,size);
  cudaMalloc((void **)&d_C,size); 

  cudaMemcpy( d_A, A, size, cudaMemcpyHostToDevice);
  int blocksize =1024;

  int dimGrid= (int)ceil((float)tamano/(float)blocksize);
  int n = tamano;
  
  reducir2<<<  dimGrid, tamano >>>(d_A, d_C, n);
  cudaMemcpy( C,d_C, size, cudaMemcpyDeviceToHost);  
  
  cudaFree(d_A);
  cudaFree(d_C);
  
}
////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////
//Funcion principal
int main(){
  int *vector;
  vector = (int*)malloc(sizeof(int));
  int *C;
  C = (int*)malloc(tamano*sizeof(int));
  int *D;
  D = (int*)malloc(tamano*sizeof(int));
  llenar(vector);
  sumar(vector);
  
  clock_t begin, end;
  double time_spent;
  begin = clock();
  reducirp(vector, C);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("\nEl resultado de la suma paralela con el algoritmo de reduccion Interleaved Addressing es: %d\n", C[0]);
  printf("  con un tiempo de %f segundos.\n",time_spent);
  
  begin = clock();
  reducirs(vector, D);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("\nEl resultado de la suma paralela con el algoritmo de reduccion Sequential Addressing es: %d\n", D[0]);
  printf("  con un tiempo de %f segundos.\n",time_spent);
  
  return 0;
  
}