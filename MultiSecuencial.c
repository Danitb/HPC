#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"

#define filasA 1
#define columA 3
#define filasB 3
#define columB 1
//#define tamano (filas*columnas)

void llenarMat(int *a, int numero){
	int i=0;
	
	while(i<numero){
		a[i]=rand()%10;
		i++;
	}
}

void multi(int *matA, int *matB, int *matC){
	int i, j, k, cont;
	clock_t begin, end;
	double time_spent;
	begin = clock();
	
	for(i=0;i<filasA;i++){
		for(j=0;j<columB;j++){
			cont=0;
			for(k=0;k<columA;k++){
				cont+= matA[i*columA+k]*matB[k*columB+j];
			}
			matC[i*columB+j]=cont;
		}
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado la multiplicacion secuencial %f segundos.\n",time_spent);
}


void mostrarMat(int *a, int f, int c){
	int i=0,j=0;
	
	for(i=0;i<f;i++){
		for(j=0;j<c;j++){
			printf("%d ", a[i*c+j]);
		}
		printf("\n");
	}
}


int main(){	
	
	int *matrizA=NULL;
	matrizA=(int *) malloc(sizeof(int)*(filasA*columA));
	int *matrizB=NULL;
	matrizB=(int *) malloc(sizeof(int)*(filasB*columB));
	int *matrizC=NULL;
	matrizC=(int *) malloc(sizeof(int)*(filasA*columB));
	
	llenarMat(matrizA, filasA*columA);
	llenarMat(matrizB, filasB*columB);
	
	multi(matrizA, matrizB, matrizC);
	
	mostrarMat(matrizA, filasA, columA);
	printf("\n");
	mostrarMat(matrizB, filasB, columB);
	printf("\n");
	mostrarMat(matrizC, filasA, columB);
	printf("\n");
	
	return 0;
}
