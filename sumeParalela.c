#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "cuda.h"
#define tamano 1024


void mostrar(float *a, float *b, float *c){
	printf("\n");
	int i=0;
	while( i<10){
		printf("%f + %f = %f\n", a[i], b[i], c[i]);
		i++;
	}
}

__global__ void vecAdd(float *A, float *B, float *C, int n){
	int i=threadIdx.x;
	//int i=blockIdx*bloxkDim+threadIdx.x;   //Cuando se van a lanzar varios bloques de hilos
	if(i<n)
		C[i]=A[i]+B[i];
}

void vectorAdd(float *A, float *B, float *C, int n){
	int size=n*sizeof(int);
	float *d_A, *d_B, *d_C;
	clock_t begin, end;
	double time_spent;
  
	begin = clock();
  
	
	cudaMalloc((void**)&d_A, size);
	cudaMalloc((void**)&d_B, size);
	cudaMalloc((void**)&d_C, size);
	

	
	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);
	
	vecAdd<<<n,1>>>(d_A, d_B, d_C, n);
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);
	end = clock();
	mostrar(A,B,C);
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Se ha demorado %f segundos.\n",time_spent);
	
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	//return 0;
}

int main(){
	int i=0;
	
	float *vectorA=NULL;
	vectorA=(float *) malloc(sizeof(float)*tamano);
	float *vectorB=NULL;
	vectorB=(float *) malloc(sizeof(float)*tamano);
	float *vectorC=NULL;
	vectorC=(float *) malloc(sizeof(float)*tamano);
	
	while(i<tamano){
		vectorA[i]=rand()%10;
		vectorB	[i]=rand()%10;
		i++;
	}
	vectorAdd(vectorA, vectorB, vectorC, tamano);
	return 0;
}
